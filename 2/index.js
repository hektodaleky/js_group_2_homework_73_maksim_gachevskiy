const express=require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const port = 8000;

const password='password';
let crypt;
app.get('/encode/:code', (req, res) => {
    crypt=Vigenere.Cipher(password).crypt(req.params.code);
    res.send(crypt)
});

app.get('/decode/:code', (req, res) => {
    crypt=Vigenere.Decipher(password).crypt(req.params.code);
    res.send(crypt)
});
app.get('/*', (req, res) => {
    res.send("Необходим GET запрос с /encode/* или /decode/*")
});
app.listen(port, () => {
    console.log('We are live on 8000 port')
});